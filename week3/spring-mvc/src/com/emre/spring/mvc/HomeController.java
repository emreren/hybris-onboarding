package com.emre.spring.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HomeController {

	@RequestMapping("/")
	public String showPage() {
		return "main-menu";
	}
	
	
	@RequestMapping("/getform")
	public String getForm() {
		return "main-form";
	}
	
	
	@RequestMapping("/showform")
	public String showForm(@RequestParam("name") String name, Model model) {
		model.addAttribute("name", name.toUpperCase());
		
		return "main-form-show";
	}
}
