package com.deloitte;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

    public static void main(String[] args) {
        
        int CASE = 5;

        switch (CASE) {
            case 0:
                caseDefault();
                break;
            case 1:
                caseLifecycle();
                break;
            case 2:
                caseScope();
                break;
            case 3:
                caseAnnotation();
                break;
            case 4:
                caseAnnotationNoXml();
                break;
            default:
                    break;

        }
    }

    private static void caseAnnotationNoXml() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(CoachConfig.class);

        SoccerCoach theCoach = context.getBean("soccerCoach", SoccerCoach.class);

        System.out.println(theCoach.getDailyWorkout());
        System.out.println(theCoach.getMessage());

        context.close();
    }

    private static void caseAnnotation() {
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("applicationContext-annotation.xml");

        Coach theCoach = context.getBean("soccerCoach", Coach.class);

        System.out.println(theCoach.getDailyWorkout());

        context.close();

    }

    public static void caseDefault() {
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("applicationContext.xml");

        Coach theCoach = context.getBean("exampleBean", Coach.class);

        System.out.println(theCoach.getDailyWorkout());

        context.close();
    }

    public static void caseLifecycle() {
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("applicationContext-lifecycle.xml");

        Coach theCoach = context.getBean("exampleBean", Coach.class);

        context.close();

    }

    public static void caseScope() {
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("applicationContext-scope.xml");

        Coach theCoach = context.getBean("exampleBean", Coach.class);

        Coach secondCoach = context.getBean("exampleBean", Coach.class);

        System.out.println(theCoach == secondCoach);


        context.close();
    }
}
