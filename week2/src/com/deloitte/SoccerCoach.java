package com.deloitte;


import org.springframework.stereotype.Component;

@Component
public class SoccerCoach implements Coach {

    Message message;

    public SoccerCoach(Message message) {
        this.message = message;
    }

    @Override
    public String getDailyWorkout() {
        return "Soccer coach";
    }

    public String getMessage() {
        return message.getMessage();
    }

    public void postConstruct(){
        System.out.println("post construct");
    }

    public void preDestroy(){
        System.out.println("pre destroy");
    }
}
