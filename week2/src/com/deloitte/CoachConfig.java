package com.deloitte;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.deloitte")
public class CoachConfig {


    @Bean
    public Coach getSoccerCoach() {
        return new SoccerCoach(getMessage());
    }

    @Bean
    public Message getMessage() {
        return new Message();
    }

}
