package com.deloitte;

public class TennisCoach implements Coach {

	String string = "not modified";
	private String privateField;

	public TennisCoach(String arg) {
		string = arg;
	}

	@Override
	public String getDailyWorkout() {
		if (privateField != null) {
			System.out.println("Private field : " + privateField);
		}

		return string;
	}

	public void setUp() {
		System.out.println("set up method");

	}

	public void tearDown() {
		System.out.println("tear down method");
	}

	public void setPrivateField(String privateField) {
		this.privateField = privateField;
	}
}
