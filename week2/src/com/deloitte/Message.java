package com.deloitte;

public class Message {

    private String message = "Message from message class";

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
